



from turtle import xcor


class Board:
    def __init__(self, index):
        self.occupied = False
        self.index = index
        self.occupied_by = None

    def __repr__(self):
        return f'{self.index}, occupied: {self.occupied}, occupied_by: {self.occupied_by}'

    

class Figures:
    def __init__(self, position, team):
        self.position = position
        self.on_board = True
        self.selected = False
        self.movable = []
        self.can_beat = False
        self.team = team
        self.super = False
        
    
    def __repr__(self):
        return f'{self.team}, position: {self.position}, movable: {self.movable}'
    


        


class Game:

    def __init__(self):
        self.teams = ['white', 'black']
        self.white = []
        self.black = []
        self.turn = self.teams[0]
        self.draw_a_game()


        # self.check_is_move_work()
        while True:
            self.clear_movable()
            self.occupied_status()
            self.check_movable_whites()
            self.check_movable_blacks()
            self.capture()
            self.make_move()
            
            
        
    def display(self):

        with open('board.txt', 'w') as file:
            counter = 0
            for element in self.spaces:
                counter += 1
                if element.occupied_by == None:
                    file.write("O")
                elif element.occupied_by == 'white':
                    file.write("W")
                elif element.occupied_by == 'black':
                    file.write("B")
                if counter % 8 == 0:
                    file.write("\n")
        
    def draw_a_game(self):
        self.create_fields()
        self.put_figures()
        self.show_borders()
        self.fields_side()
        

    def create_fields(self):
        self.spaces = []
        index = 0
        for x in range(64):
            self.spaces.append(Board(index))
            index += 1


    def fields_side(self):
        self.centre = (18,19,20,21,26,27,28,29,34,35,36,37,42,43,44,45)
        self.north = (2,3,4,5,10,11,12,13)
        self.south = (50,51,52,53,58,59,60,61)
        self.east = (22,23,30,31,38,39,46,47)
        self.west = (16,17,24,25,32,33,40,41)
        self.north_east = (6,7,14,15)
        self.north_west = (0,1,8,9)
        self.south_east = (54,55,62,63)
        self.south_west = (48,49,56,57)


    def show_borders(self):
        self.left_side = []
        self.right_side = []
        for element in range(64):
            if element % 8 == 0:
                self.left_side.append(element)
                self.right_side.append(element+7)
        

    def put_figures(self):
        row = 0 
        for space in self.spaces[0:24]:
            if space.index % 8 == 0:
                row += 1
            if row%2 == 0 and space.index % 2 == 0:
                self.white.append(Figures(space.index, self.teams[0]))
                self.spaces[space.index].occupied_by = 'white'
            elif row%2 == 1 and space.index % 2 == 1:
                self.white.append(Figures(space.index, self.teams[0]))
                self.spaces[space.index].occupied_by = 'white'
            

        row = 1
        for space in self.spaces[-24:]:
            if space.index % 8 == 0:
                row += 1
            if row%2 == 0 and space.index % 2 == 0:
                self.black.append(Figures(space.index, self.teams[1]))
                self.spaces[space.index].occupied_by = 'black'
            elif row%2 == 1 and space.index % 2 == 1:
                self.black.append(Figures(space.index, self.teams[1]))
                self.spaces[space.index].occupied_by = 'black'
        
    
        self.all_figures = self.white + self.black

    def occupied_status(self):
        for field in self.spaces:
            for figure in self.white + self.black:
                if field.index == figure.position:
                    field.occupied = True
                    field.occupied_by = figure.team
                    break
                else:
                    field.occupied = False
                    field.occupied_by = None
    

    def clear_movable(self):
        for figure in self.white + self.black:
            figure.movable = []

    def check_movable_whites(self):
        for element in self.white:
            if element.position not in self.left_side + self.right_side:
                if self.spaces[element.position+7].occupied == False or self.spaces[element.position+9].occupied == False:
                    if self.spaces[element.position+7].occupied == False:
                        element.movable.append(self.spaces[element.position+7].index)
                    if self.spaces[element.position+9].occupied == False:
                        element.movable.append(self.spaces[element.position+9].index)
                else:
                    element.movable = []
                
            elif element.position in self.right_side:
                if self.spaces[element.position+7].occupied == False:
                    element.movable.append(self.spaces[element.position+7].index)
                else:
                    element.movable = []
                
            else:
                if self.spaces[element.position+9].occupied == False:
                    element.movable.append(self.spaces[element.position+9].index)
                else:
                    element.movable = []
    


    def check_movable_blacks(self):
        for element in self.black:
            if element.position not in self.left_side + self.right_side:
                if self.spaces[element.position-7].occupied == False or self.spaces[element.position-9].occupied == False:
                    if self.spaces[element.position-7].occupied == False:
                        element.movable.append(self.spaces[element.position-7].index)
                    if self.spaces[element.position-9].occupied == False:
                        element.movable.append(self.spaces[element.position-9].index)
                else:
                    element.movable = []
                
            elif element.position in self.right_side:
                if self.spaces[element.position-9].occupied == False:
                    element.movable.append(self.spaces[element.position-9].index)
                else:
                    element.movable = []

            else:
                if self.spaces[element.position-7].occupied == False:
                    element.movable.append(self.spaces[element.position-7].index)
                else:
                    element.movable = []
    


    def make_move(self):

        def check_is_beated(position, move):
            sums = (-9,-7,7,9)
            if position - move not in sums: 
                return True
            else:
                return False
            


        self.display()

        
        print('\n=====================')
        print(f'{self.turn}\'s move')


        print("Choose one of your pawn that can move:")
        print('=====================\n')
        

        movable = []
        who_next = ['white', 'black']
        can_beat = []
        beaten_position = 0

    
        i = -1
        for figure in self.white + self.black:
            if figure.on_board == True and figure.team == self.turn and len(figure.movable)>0:
                if figure.can_beat == True:
                    for element in figure.movable:
                        if check_is_beated(figure.position, element):
                            can_beat.append(element)
                movable.append(figure)
                i += 1
                print(f'[{i}] - {figure}' )
                
        

        # ---------------------Pick and move -------------------
        select = input("chose: ")
        print('where you want to move?')
        for move in movable[int(select)].movable:
            print(move)
        move = int(input())




        if move in movable[int(select)].movable and move not in can_beat:
            for figure in self.white + self.black:
                if figure.position == movable[int(select)].position:
                    figure.position = move
            who_next.remove(self.turn)
            self.turn = who_next[0]


        elif move in can_beat:
            for figure in self.white + self.black:
                if figure.position == movable[int(select)].position:
                    beaten_position = figure.position + ((move-figure.position)/2)
                    figure.position = move
            
            for figure in self.white + self.black:
                if figure.position == beaten_position:
                    figure.on_board = False
                    figure.position = -1

            who_next.remove(self.turn)
            self.turn = who_next[0]

        else:
            print('\n------------------')
            print('You Cant Move There')
            print('Try again')
            print('------------------\n')
                
        


    def capture(self):
        teams = ['white', 'black']
        teams.remove(self.turn)
        opponent = teams[0]
        
        for figure in self.white + self.black:
            if figure.team == self.turn:
                if figure.position in self.north:
                    if self.spaces[figure.position + 9].occupied_by == opponent and self.spaces[figure.position + 18].occupied == False:
                            figure.movable.append(self.spaces[figure.position + 18].index)
                            figure.can_beat = True
                    if self.spaces[figure.position + 7].occupied_by == opponent and self.spaces[figure.position + 14].occupied == False:
                            figure.movable.append(self.spaces[figure.position + 14].index)
                            figure.can_beat = True

                if figure.position in self.north_west:
                    if self.spaces[figure.position + 9].occupied_by == opponent and self.spaces[figure.position + 18].occupied == False:
                            figure.movable.append(self.spaces[figure.position + 18].index)
                            figure.can_beat = True

                if figure.position in self.north_east:     
                        if self.spaces[figure.position + 7].occupied_by == opponent and self.spaces[figure.position + 14].occupied == False:
                            figure.movable.append(self.spaces[figure.position + 14].index)
                            figure.can_beat = True

                if figure.position in self.west:     
                    if self.spaces[figure.position - 7].occupied_by == opponent and self.spaces[figure.position - 14].occupied == False:
                            figure.movable.append(self.spaces[figure.position - 14].index)
                            figure.can_beat = True
                    if self.spaces[figure.position + 9].occupied_by == opponent and self.spaces[figure.position + 18].occupied == False:
                            figure.movable.append(self.spaces[figure.position + 18].index)
                            figure.can_beat = True

                if figure.position in self.centre:
                    if self.spaces[figure.position + 9].occupied_by == opponent and self.spaces[figure.position + 18].occupied == False:
                            figure.movable.append(self.spaces[figure.position + 18].index)
                            figure.can_beat = True
                    if self.spaces[figure.position + 7].occupied_by == opponent and self.spaces[figure.position + 14].occupied == False:
                        figure.movable.append(self.spaces[figure.position + 14].index)
                        figure.can_beat = True
                    if self.spaces[figure.position - 7].occupied_by == opponent and self.spaces[figure.position - 14].occupied == False:
                        figure.movable.append(self.spaces[figure.position - 14].index)
                        figure.can_beat = True
                    if self.spaces[figure.position - 9].occupied_by == opponent and self.spaces[figure.position - 18].occupied == False:
                        figure.movable.append(self.spaces[figure.position - 18].index)
                        figure.can_beat = True

                if figure.position in self.east:
                    if self.spaces[figure.position - 9].occupied_by == opponent and self.spaces[figure.position - 18].occupied == False:
                        figure.movable.append(self.spaces[figure.position - 18].index)
                        figure.can_beat = True
                    if self.spaces[figure.position + 7].occupied_by == opponent and self.spaces[figure.position + 14].occupied == False:
                            figure.movable.append(self.spaces[figure.position + 14].index)
                            figure.can_beat = True

                if figure.position in self.south_west:
                    if self.spaces[figure.position - 7].occupied_by == opponent and self.spaces[figure.position - 14].occupied == False:
                        figure.movable.append(self.spaces[figure.position - 14].index)
                        figure.can_beat = True
                        
                if figure.position in self.south:
                    if self.spaces[figure.position - 7].occupied_by == opponent and self.spaces[figure.position - 14].occupied == False:
                        figure.movable.append(self.spaces[figure.position - 14].index)
                        figure.can_beat = True
                    if self.spaces[figure.position - 9].occupied_by == opponent and self.spaces[figure.position - 18].occupied == False:
                        figure.movable.append(self.spaces[figure.position - 18].index)
                        figure.can_beat = True

                if figure.position in self.south_east:
                    if self.spaces[figure.position - 9].occupied_by == opponent and self.spaces[figure.position - 18].occupied == False:
                            figure.movable.append(self.spaces[figure.position - 18].index)
                            figure.can_beat = True

                        
                    

                    
                



        



    

            

                

Game()